<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Photo;
use App\Models\Tag;

use App\Repositories\TagRepository;
use App\Repositories\PhotoRepository;

class PhotoController extends Controller
{

    public function store(Request $request)
    {
        $request->validate([
            'photo' => 'required|image',
            'tags'  => 'required'
        ]);

        $photo = PhotoRepository::storeAndCreate($request->file('photo'));
        $tags  = TagRepository::createAndCheck($request->get('tags'));

        $photo->tags()->attach($tags);

        return back();
    }

    public function addTag(Request $request, Photo $photo)
    {
        $tags = TagRepository::createAndCheck($request->get('tags'));

        $photo->tags()->attach($tags);

        return back();
    }

    public function removeTag(Photo $photo, Tag $tag)
    {
        $photo->tags()->detach($tag);

        return back();
    }

    public function searchByTag(Request $request)
    {
        $photos = Photo::searchByTag($request->get('search'));

        return back()->with('photos', $photos);
    }
}
