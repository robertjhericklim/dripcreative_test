<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Tag;

class TagController extends Controller
{

    public function update(Request $request, Tag $tag)
    {
        $tag->update([
            'keyword' => $request->get('keyword')
        ]);

        return back();
    }

    public function destroy(Tag $tag)
    {
        $tag->delete();

        return back();
    }
}
