<?php

namespace App\Repositories;

use App\Models\Tag;
use Illuminate\Support\Facades\DB;

class TagRepository
{
    /**
     * Create a tag only if it exists. If not, pass in the result of the tag
     * on the result
     *
     * @param string $data
     * 
     * @return object 
     */
    public static function createAndCheck(string $data)
    {
        $tags = array_map('trim', explode(',', $data));

        $result = [];
        DB::transaction(function () use ($tags, &$result) {
            foreach($tags as $tag) {
                $data = [
                    'keyword' => $tag
                ];

                $result[] = Tag::firstOrCreate($data)->id;
            }
        });
        
        return $result;
    }
}