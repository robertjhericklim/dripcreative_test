<?php

namespace App\Repositories;

use App\Models\Photo;

class PhotoRepository
{
    /**
     * Uploads the image in storage and stores the path 
     * in the database
     *
     * @param object $data
     * @param string $filename
     * 
     * @return object
     */
    public static function storeAndCreate(object $data, string $filename = null)
    {
        $path = ($filename) 
            ? $data->storeAs('photo', now()->timestamp . '-' . $filename, 'public')
            : $data->store('photo', 'public');

        $photo = Photo::create([
            'path' => $path
        ]);

        return $photo;
    }
}