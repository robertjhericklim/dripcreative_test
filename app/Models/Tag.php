<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['keyword'];

    public $timestamps = false;

    public function photos()
    {
        return $this->belongsToMany(Photo::class);
    }
}
