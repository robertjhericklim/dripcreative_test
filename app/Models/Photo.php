<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = ['path'];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public static function searchByTag($searchString)
    {
        return static::whereHas('tags', function ($query) use ($searchString) {
            $query->where('keyword', 'like', '%'.$searchString.'%');
        })->get();
    }
}