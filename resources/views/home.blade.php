@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form action="{{ route('photos.store') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="">Upload Image:</label>
                        <input type="file" class="form-control-file" name="photo" >
                    </div>

                    <div class="form-group">
                        <label for="tags">Add Tags to image:</label>
                        <small>Separated by comma</small>

                        <input type="text" class="form-control" name="tags" id="tags" value="{{ old('tags') }}">
                    </div>
                    
                    <button class="btn btn-primary" type="submit">Submit</button>
                </form>

                <br>

                @include('partials.errors')
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-md-12">
                <form action="{{ route('photos.search-by-tag') }}" method="post">
                    {{ csrf_field() }}
                    
                    <div class="form-group">
                        <label for="">Search by tag:</label>
                        <input type="text" class="form-control" name="search">
                    </div>

                    <button class="btn btn-primary" type="submit">Search</button>
                </form>
            </div>
        </div>

        <div class="row mt-3">
            @if (session()->has('photos'))
                @foreach (session('photos') as $photo)
                    <div class="col-md-3">
                        <img src="{{ Storage::url($photo->path) }}" class="img-fluid" style="width: 100%" alt="">

                        @foreach ($photo->tags as $tag)
                            <span class="badge badge-secondary">
                                {{ $tag->keyword }}
                                <form action="{{ route('photos.remove-tag', [$photo->id, $tag->id]) }}" method="post">
                                    {{ method_field('PATCH') }}
                                    {{ csrf_field() }}

                                    <button type="submit" class="btn btn-secondary">X</button>
                                </form>
                            </span>
                        @endforeach

                        <form action="{{ route('photos.add-tag', $photo->id) }}" method="post">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="tags">Add Tags to image:</label>
                                <small>Separated by comma</small>
                                
                                <input type="text" class="form-control" name="tags" id="tags" value="{{ old('tags') }}">
                            </div>

                            <button class="btn btn-primary" type="submit">Add</button>
                        </form>
                    </div>
                @endforeach
            @else
                @foreach ($photos as $photo)
                    <div class="col-md-3">
                        <img src="{{ Storage::url($photo->path) }}" class="img-fluid" style="width: 100%" alt="">

                        @foreach ($photo->tags as $tag)
                            <span class="badge badge-secondary">
                                {{ $tag->keyword }}
                                <form action="{{ route('photos.remove-tag', [$photo->id, $tag->id]) }}" method="post">
                                    {{ method_field('PATCH') }}
                                    {{ csrf_field() }}

                                    <button type="submit" class="btn btn-secondary">X</button>
                                </form>
                            </span>
                        @endforeach

                        <form action="{{ route('photos.add-tag', $photo->id) }}" method="post">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="tags">Add Tags to image:</label>
                                <small>Separated by comma</small>
                                
                                <input type="text" class="form-control" name="tags" id="tags" value="{{ old('tags') }}">
                            </div>

                            <button class="btn btn-primary" type="submit">Add</button>
                        </form>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection

@push('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.tags-editable').editable();
        });
    </script>
@endpush

@push('styles')
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/jquery-editable/css/jquery-editable.css" rel="stylesheet"/>
@endpush
