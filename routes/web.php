<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'HomeController@index');

Route::resource('photos', 'PhotoController')->only('store');
Route::post('photos/search-by-tag', 'PhotoController@searchByTag')->name('photos.search-by-tag');
Route::patch('photos/{photo}/add-tag/', 'PhotoController@addTag')->name('photos.add-tag');
Route::patch('photos/{photo}/remove-tag/{tag}', 'PhotoController@removeTag')->name('photos.remove-tag');

Route::resource('tags', 'TagController')->only('store', 'update', 'destroy');

